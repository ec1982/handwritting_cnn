# handwritting_CNN

Chinese handwriting recognize using CNN model

**目錄結構說明:**

- model:存放.h5的權重及訓練模型，可直接匯入

- train_set(目前是空的):存放訓練集及驗證集圖檔(50x50 pixel)，結構如下<br>
    人(資料夾):人01.png,人02.png...<br>
    不(資料夾):不01.png,不02.png...<br>

- test_set(目前是空的):存放測試集圖檔(50x50 pixel)，結構如下<br>
    人(資料夾):人01.png,人02.png...<br>
    不(資料夾):不01.png,不02.png...<br>

**使用說明**

將此資料專案整批下載至本機，再上傳至google driver第一層目錄
